#include <stdio.h>

int main() {
    printf("Which number shall we query? > ");  // get user input
    int user_input;                             // here is where user input is stored
    scanf("%i",&user_input);

    printf("%d%s",user_input," is ");           // Concatenates outputs: %d is
    if(user_input < 0) {
        printf("%s","negative");                // %d is negative
    } else if(user_input > 0) {
        printf("%s","positive");                // %d is positive
    } else {
        printf("%s\n","0");
        return 0;
    }
    printf("%s"," and ");                       // %d is positive and
    if(user_input%2 == 0) {
        printf("%s\n","even");                  // %d is positive and even
    } else {
        printf("%s\n","odd");                   // %d is positive and odd
    }
    return 0;
}
