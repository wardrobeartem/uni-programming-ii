#include <stdio.h>
#include <string.h>

#define SIZE 5

typedef char string [40];

typedef struct {
    string  name;
    char    gender;
    size_t  age;   //size_t = unsigned long
    float   grade;
} Student;

Student class[SIZE];

int main() {

    printf("Please enter data in the form:\nName Surname Gender Age Grade\n");

    for(int i = 0; i < SIZE; i++) {
        Student newStudent;
        string surname;

        printf("\nPlease provide the data for student #%d:\n", i+1);
        scanf("%s%s%s%zd%f",newStudent.name,surname,&(newStudent.gender),&(newStudent.age),&(newStudent.grade));
        strcat(newStudent.name," ");
        strcat(newStudent.name,surname); // combine name and surname into 1 string

        class[i] = newStudent;
    }

    // here I would've printed the stuff if you asked it to be printed

    return 0;
}
