/*
 * QUIZ!!!!
 * Calculate an expression and print the answer
 * Usage:
 *   ./<program_name> <long operand_a> <char operator> <long operand_b>
 * Output:
 *   the evaluated expression
 * Supported operators:
 * - Addition:          +
 * - Subtraction:       -
 * - Multiplication:    x, X
 * - Division:          /, \
 * - Modulo:            %
 * Notes:
 *   Providing non-integer operands in not supported and interprited as 0
 *   Trying to divide by 0 causes an exception
 */

#include <stdio.h>                  // Used for printing the output
#include <stdlib.h>                 // Used for str -> long conversion

int main(int argc, char *argv[]) {

    /* initialise variables */
    long a, b;
    char operator;

    /* Check for correct number of arguments provided */
    if(argc != 4) {
        printf("Wrong amount of arguments provided. Try: ./programm_name <a> <operator> <b>\n");
        return 1;
    }

    /* Store command line arguments to variables */
    a = strtold(argv[1],NULL);
    operator = *argv[2];
    b = strtold(argv[3],NULL);

    /* Compute and print the result */
    switch(operator) {
        case '+':
            printf("%ld + %ld = %ld\n",a,b,a+b);
            break;
        case '-':
            printf("%ld - %ld = %ld\n",a,b,a-b);
            break;
        case 'x':
        case 'X':
            printf("%ld × %ld = %ld\n",a,b,a*b);
            break;
        case '/':
        case '\\':
            if(b==0) {    // check for div by 0 error
                printf("Division by 0 error\n");
                return 1;
            }
            printf("%ld ÷ %ld = %g\n",a,b,(float)a/b);
            break;
        case '%':
            printf("The remainder of %ld ÷ %ld = %ld\n",a,b,a%b);
            break;
        default:
            printf("Unknown operator\n");
            return 1;
    }

    return 0;
}
