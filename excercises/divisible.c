#include <stdio.h>

const int MIN               = 1;
const int MAX               = 20;
const int DIVISORS[2]       = {2,3};
const int DIVISORS_LENGTH   = sizeof(DIVISORS)/sizeof(DIVISORS[0]);
const int BLACKLISTED_VALUE = 12;
const int BREAKOUT_VALUE    = 15;

/*
 * check divisibility of a number
 * input: int you want to check
 * output: int 1, if divisible
 * int 0 otherwise
 */
int checkDivisibility(int a) {
    for(int j = 0; j <= DIVISORS_LENGTH - 1; j++) {
        if(a % DIVISORS[j] != 0) {
            return 0;
        }
    }
    return 1;
}

int main() {
    for(int i = MIN; i <= MAX; i++)  {
        if(i == BLACKLISTED_VALUE) {
            continue;
        }
        if(i == BREAKOUT_VALUE) {
            break;
        }
        if(checkDivisibility(i) == 1) {
            printf("%d; ",i);
        }
    }
    printf("\n");
    return 0;
}

