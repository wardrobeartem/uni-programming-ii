#include <stdio.h>  // used for printing the output

const int MIN = 1;  // define constants
typedef short bool; // rename short to bool

/* Calculates the sum according to the provided parameterizedSum
 * Inputs:
 *  (int) min: the lower limit of the sum (inclusive)
 *  (int) max: the upper limit of the sum (inclusive)
 *  (function pointer) modifier: each number would be first modified using
 *                               this function before being added to the sum
 *  (function pointer) filter: each number would only be added to the sum if
 *                             it satisfies the filter criteria
 * Output:
 *  The sum of all numbers from min to max that fit the criteria,
 *  and that have been modified
 * Notes:
 *  The check using the filter function is applied before the modifier,
 *  so if the number passes the filter only before being modified, it would still
 *  be added to the sum
 */
long parameterizedSum(int min, int max, long (*modifier)(long), bool (*filter)(long)) {
    int sum = 0;
    for (int i = min; i <= max; i++) {
        if((*filter)(i) == 1) {
            sum += (*modifier)(i);
        }
    }
    return sum;
}

// Returns the provided number
long unchanged(long n) {
    return n;
}

// Returns the provided number squared
long squared(long n) {
    return n*n;
}

// Always returns 1
bool true(long n) {
    return 1;
}

// Returns 1 if the number is odd, 0 otherwise
bool isOdd(long n) {
    return (n % 2);
}

// Returns 1 if the number is even, 0 otherwise
bool isEven(long n) {
    return ((n+1) % 2);
}

int main(int argc, char *argv[]) {
    int max;
    printf("Please provide the upper limit for the calculation: ");
    scanf("%d",&max);   // grab user input

    if(max <= 0) {      // check for edge cases
        printf("Please provide a natural number\n");
        return 1;
    }

    printf("The sum of all numbers from %d to %d =\t\t%ld\n",MIN,max,parameterizedSum(MIN,max,unchanged,true));
    printf("The sum of numbers squared from %d to %d =\t%ld\n",MIN,max,parameterizedSum(MIN,max,squared,true));
    printf("The sum of odd numbers from %d to %d = \t\t%ld\n",MIN,max,parameterizedSum(MIN,max,unchanged,isOdd));
    printf("The sum of even numbers from %d to %d =\t\t%ld\n",MIN,max,parameterizedSum(MIN,max,unchanged,isEven));
    return 0;
}
