#include <stdio.h>
#include <stdlib.h>

#define SIZE 6
#define FILE_NAME "Numbers.txt"

typedef char string [12];
void populate_array_from_file(int[], int, string);
void print_array(int[], int);
void sort_array(int[], int);

int main() {

    // define an array to hold 6 numbers
;;;;int numsArray[SIZE];

    // read the numbers from file Numbers.txt to fill the array
;;;;populate_array_from_file(numsArray, SIZE, FILE_NAME);


    // print the unsorted array
;;;;print_array(numsArray, SIZE);

    // sort the array in place
;;;;sort_array(numsArray, SIZE);

    // print the sorted array
;;;;print_array(numsArray, SIZE);

    return 0;
}

void populate_array_from_file(int array[], int length, string file_name) {
    FILE *f;
    f = fopen (file_name, "r");
    string tmp;
    for(int i = 0; i < length; i++) {
        fgets(tmp, 10, f);
        array[i] = strtold(tmp,NULL);
    }
}

void print_array(int array[], int length) {
    printf("[");
    for (int i = 0; i < length; i++) {
        printf(" %d",array[i]);
    }
    printf(" ]\n");
}

void sort_array(int array[], int length) {
    int tmp;
    for (int i = 0; i < length; i++) {
        for (int j = i+1; j < length; j++) {
            if(array[i] > array[j]) {
                tmp = array[j];
                array[j] = array[i];
                array[i] = tmp;
            }
        }
    }

}
