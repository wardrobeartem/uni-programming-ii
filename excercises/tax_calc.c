/*
    Calculate the worker's net income based on some parameters
    Input(s):   number of hours worked, number of kids
    Output(s):  net income with all details
    Remarks:
        0. All values are hardcoded
        1. Tax calculation only works for 2 threshold values 0-lower lower-upper upper-∞
*/

#include <stdio.h>

// Define all constants
const int   CHILD_ALLOWANCE     = 100;
const float HOURLY_WAGE         = 20.0;
const int   OVERTIME_THRESHOLD  = 40;
const float OVERTIME_BONUS      = 0.3;
const float TAX_THRESHOLD[2]    = {1000,1500};
const float TAX_RATE[3]         = {0.15,0.20,0.25};
const char  CURRENCY_SYMBOL     = '$';

int main() {

    // Define variables
    int numberOfHours, numberOfChildren;
    float grossIncome, taxableIncome, tax, netIncome;

    // Get data from the user
    printf("How many hours did you work? > ");
    scanf("%i",&numberOfHours);
    printf("How many kids do you have? > ");
    scanf("%i",&numberOfChildren);

    // Calculate gross income
    if (numberOfHours <= OVERTIME_THRESHOLD) {
        grossIncome = numberOfHours * HOURLY_WAGE;
    } else {
        grossIncome = (OVERTIME_THRESHOLD * HOURLY_WAGE) + ((numberOfHours - OVERTIME_THRESHOLD) * HOURLY_WAGE * (1 + OVERTIME_BONUS));
    }

    // Deduct child allowance from taxable income
    taxableIncome = grossIncome - (numberOfChildren * CHILD_ALLOWANCE);

    // Compute tax
    if (taxableIncome < TAX_THRESHOLD[0]) {
        tax = taxableIncome * TAX_RATE[0];
    } else if (taxableIncome < TAX_THRESHOLD[1]) {
        tax = taxableIncome * TAX_RATE[1];
    } else {
        tax = taxableIncome * TAX_RATE[2];
    }

    // Calcultte the net income
    netIncome = grossIncome - tax;

    // print the stuff
    printf("Your gross income is:\t%c%.2f\n",CURRENCY_SYMBOL,grossIncome);
    printf("Your taxable income is:\t%c%.2f\n",CURRENCY_SYMBOL,taxableIncome);
    printf("Your tax is:\t\t%c%.2f\n",CURRENCY_SYMBOL,tax);
    printf("Your net income is:\t%c%.2f\n",CURRENCY_SYMBOL,netIncome);

    return 0;
}
