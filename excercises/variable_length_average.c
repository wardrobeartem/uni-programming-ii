/*
 * BOOO
 * Take the average of numbers provided as the command line arguments
 */

#include <stdio.h>                  // Used for printing the output
#include <stdlib.h>                 // Used for str -> double conversion
#include <stdarg.h>                 // Used for Variable Length Parameter Lists

#define ARGUMENTS_0 arguments[0]

/* Computes the average of provided numbers
 * Arguments:
 *  int count: the number of arguments provided excluding this one
 *  float ...: all the numbers the avg of which should be computed
 * Return:
 *  double average of the arguments
 * Notes:
 *  Will return nan if count == 0
 *  Will return nan if count > number of arguments
 *  If the number of arguments < count, the later arguments will be ignored
 *  Unexpected results if the arguments are not floats
 */
double avg(int count, ...) {
    double sum = 0;
    va_list parameters;
    va_start(parameters, count);
    for(int i = 0; i < count; ++i) {
        sum += va_arg (parameters, double);
    }
    return sum / count;
}

int main(int argc, char *argv[]) {

    /* Check if at least 1 argument is provided */
    if(argc == 1) {
        printf("Average calculator\nUsage: %s [number0] [number1] ...\n", argv[0]);
        exit(-1);
    }

    size_t numberOfNumbers = --argc;
    double arguments[numberOfNumbers], average;

    for(size_t i = 0; i < numberOfNumbers; ++i) {
        arguments[i] = strtold(argv[i+1],NULL);
    }

    switch(numberOfNumbers) {
        case 1: average = avg(1, arguments[0]); break;
        case 2: average = avg(2, arguments[0], arguments[1]); break;
        case 3: average = avg(3, arguments[0], arguments[1], arguments[2]); break;
        case 4: average = avg(4, arguments[0], arguments[1], arguments[2], arguments[3]); break;
        default: printf("Ayo choomba soooo many arguments! (%zu) We only accept maximum 4!\n",numberOfNumbers); exit(-1);
    }

    printf("Average: %g\n", average);

    return 0;
}
