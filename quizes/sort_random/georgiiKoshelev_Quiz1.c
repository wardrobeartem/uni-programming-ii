#include <stdio.h>                  // Used for printing the output
#include <stdlib.h>                 // Used for random number generation
#include "georgiiKoshelevLibrary.h" // Has all the functions there

// Declare constants
const int ARRAY_LENGTH = 6;
const int RANDOM_MAX = 49;
const int RANDOM_MIN = 1;

int main(int argc, char *argv[]) {

    // define an array to hold 6 numbers
    int random_numbers[ARRAY_LENGTH];

    // call number generator to populate the array with 6 unique numbers between 1 and 54
    populate_array(random_numbers, ARRAY_LENGTH, RANDOM_MIN, RANDOM_MAX);

    // print the unsorted array
    print_array(random_numbers, ARRAY_LENGTH);

    // sort the array in place
    sort_array(random_numbers, ARRAY_LENGTH);

    // print the sorted array
    print_array(random_numbers, ARRAY_LENGTH);

    return 0;
}


