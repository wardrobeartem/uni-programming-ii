#include "GeorgiiKoshelev_BCS-11a_Quiz3.h"

const int   MIN  = -2500;
const int   MAX  =  2500;
const float STEP =  0.25;

int main() {
    int number_of_values = (int) ((MAX-MIN)/STEP+1);
    double values[number_of_values];
    for(int i = 0; i < number_of_values; i++) {
        values[i] = f(i*STEP+MIN);
    }
    return 0;
}
