#include <stdio.h>

int fibonachi(int);

int main(int argc, char *argv[]) {
    printf("%d\n",fibonachi(1));
    printf("%d\n",fibonachi(5));
    printf("%d\n",fibonachi(6));
    printf("%d\n",fibonachi(7));

    return 0;
}

int fibonachi(int n) {
    if(n == 0) {
        return 0;
    } else if (n == 1) {
        return 1;
    } else {
        return fibonachi(n-1) + fibonachi(n-2);
    }
}
