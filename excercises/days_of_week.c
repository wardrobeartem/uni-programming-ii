#include <stdio.h>

int main() {
    const char DAYS_OF_WEEK[7][25] = {"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"}; // The array of weekdday names

    printf("Which day shall we print? > ");
    int user_input;                             // here is where user input is stored
    scanf("%i",&user_input);                    // get input from the user
    user_input--;                               // since array start at 0, but user input at 1, this fixes
    if(user_input<0 || user_input>6) {          // this handles if the user fucks up the input
        printf("Bad input provided :( Next time do better\n");
        return 1;
    }
    printf("%s\n",DAYS_OF_WEEK[user_input]);    // prints the selected thingie
    return 0;
}
