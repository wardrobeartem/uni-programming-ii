/*
 * BOOO
 * Take the average of numbers provided as the command line arguments
 */

#include <stdio.h>                  // Used for printing the output
#include <stdlib.h>                 // Used for str -> long conversion

int main(int argc, char *argv[]) {

    /* Check if at least 1 argument is provided */
    if(argc == 1) {
        printf("Average calculator\nUsage: %s [number0] [number1] ...\n", argv[0]);
        exit(-1);
    }

    size_t numberOfNumbers = --argc;
    //int arguments[numberOfNumbers];
    int sum = 0;

    for(size_t i = 0; i < numberOfNumbers; ++i) {
        //arguments[i] = strtol(argv[i+1],NULL,10);
        //printf("Number #%-lu:\t%ld\n",i+1,strtol(argv[i+1],NULL,10));
        sum += strtol(argv[i+1],NULL,10);
    }

    printf("Average: %g\n", (float) sum/numberOfNumbers);

    return 0;
}
