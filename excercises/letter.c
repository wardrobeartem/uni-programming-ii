/*
 * Converts uppercase letters to lowercase and vice-versa
 * Unexpected output for non-alphabet characters
 */


#include <stdio.h>

int main() {
    printf("Which letter shall we convert? > ");    // ask the user

    char user_input;                                // get user input
    scanf("%c",&user_input);

    if(user_input >= 'Z') {                         // If uppercase..
        user_input -= 32;                           // convert to lowercase,
    } else {                                        // else..
        user_input += 32;                           // convert to uppercase
    }

    printf("The corresponding letter is: %c\n",user_input);
    return 0;
}
