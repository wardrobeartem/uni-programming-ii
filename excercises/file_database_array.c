#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define FILE_NAME           "Student_Data.txt"
#define FIELD_SEPARATOR     ";"
#define ADDRESS_SEPARATOR   ","
#define EOL_SEPARATOR       "."
#define ARRAY_LENGTH        100

// street, number, zip, city, country

typedef char long_string [100];
typedef char string [20];

typedef struct{
    string street;
    unsigned int number;
    string zip;             //its not int for this reason: 04322-1424 <- this starts with 0 and has - in the middle
    string city;
    string country;
} Address;

typedef struct {
    string name;
    string lastname;
    unsigned int age;
    char gender;
    Address address;
} Person;

//function prototypes
Person parse_person(long_string);
Address parse_address(long_string);
void print_person(Person);
void print_people(Person*,int);
bool filter_person(Person*);

int main() {
    FILE *f = fopen(FILE_NAME,"r");
    if(f == NULL) {
        printf("couldn't open the file :(\n");
        exit(-1);
    }
    Person students[ARRAY_LENGTH];
    int length;
    long_string file_line;

    for(int i = 0; fgets(file_line, 99, f) != NULL; i++) {
        if(i==0)
            continue;
        Person new_person = parse_person(file_line);
        students[i-1] = new_person;
        length++;
    }

    print_people(students,length);
    fclose(f);

    return 0;
}

Person parse_person(long_string line) {
    Person new_person;
    char* token;
    token = strtok(line,FIELD_SEPARATOR);
    strcpy(new_person.name,token);
    token = strtok(NULL,FIELD_SEPARATOR);
    strcpy(new_person.lastname,token);
    token = strtok(NULL,FIELD_SEPARATOR);
    new_person.age = strtold(token,NULL);
    token = strtok(NULL,FIELD_SEPARATOR);
    new_person.gender = token[0];
    token = strtok(NULL,EOL_SEPARATOR);
    Address address = parse_address(token);
    new_person.address = address;
    return new_person;
}

Address parse_address(long_string line) {
    Address new_address;
    char* token;
    token = strtok(line,ADDRESS_SEPARATOR);
    strcpy(new_address.street,token);
    token = strtok(NULL,ADDRESS_SEPARATOR);
    new_address.number = strtold(token,NULL);
    token = strtok(NULL,ADDRESS_SEPARATOR);
    strcpy(new_address.zip,token);
    token = strtok(NULL,ADDRESS_SEPARATOR);
    strcpy(new_address.city,token);
    token = strtok(NULL,ADDRESS_SEPARATOR);
    strcpy(new_address.country,token);
    return new_address;
}

void print_person(Person person) {
    printf("%s ",person.name);
    printf("%s ",person.lastname);
    printf("%d ",person.age);
    printf("%c ",person.gender);
    printf("%s %d %s %s %s\n",person.address.street,person.address.number,person.address.zip,person.address.city,person.address.country);
}

void print_people(Person* students,int length) {
    unsigned int number_people_printed = 0;
    for(int i = 0; i < length; i++) {
        if(!filter_person(&students[i])) {
            print_person(students[i]);
            number_people_printed++;
        }
    }
    if(number_people_printed == 0)
        printf("There is noone matching the criteria :(\n");
}

// returns true if the person should be filtered out from the output
bool filter_person(Person* person) {
    /*
    if(!(person->gender == 'M'))
        return true;
    if((strcmp(person->name,"Georgii")))
        return true;
    */
    return false;
}
