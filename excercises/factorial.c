#include <stdio.h>
#include "factorial_lib.h"

unsigned long long factorial_loop(int a) {
    unsigned long long factorial = 1;
    if(a <= 1) {
        return 1;
    }
    for(long long i = 1; i <= a; i++) {
        factorial = factorial * i;
    }
    return factorial;
}

unsigned long long factorial_recursive(int a) {
    if(a <= 1) {
        return 1;
    }
    return factorial_recursive(a-1)*a;
}

unsigned long long factorial_after_body(int);

int main(int argc, char const *argv[])
{

    printf("%llu\n", factorial_loop(5));
    printf("%llu\n", factorial_recursive(7));
    printf("%llu\n", factorial_after_body(9));
    printf("%llu\n", factorial_header(11));

    return 0;
}

unsigned long long factorial_after_body(int a) {
    if(a <= 1) {
        return 1;
    }
    return factorial_recursive(a-1)*a;
}
