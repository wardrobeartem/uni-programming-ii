#include <stdio.h>
#define AMOUNT 10

int solution_with_arrays();
int solution_without_arrays();

/* Gets the user input and describes it
 *
 * Inputs:
 *  10 numbers from STDIN (all intigers)
 *  eg. 1 2 3 4 5 6 7 8 9 10
 *
 * Output:
 *  The sum of the numbers provided (int)
 *  The minimum number provided (int)
 *  The maximum number provided (int)
 *  The average of the numbers provided (float)
 *  All to the STDOUT
 *
 * Notes:
 *  If 11, or more numbers are entered, the later ones are disregarded
 *  big numbers might be too big
 *  to select which solution you want to run just comment out the other one :)
 */
int main() {
    return solution_with_arrays();
  //return solution_without_arrays();
}

int solution_with_arrays() {

    // Declare the variables
    int sum=0, min, max, numbers[AMOUNT];
    float avg;

    // Get the values from the user
    printf("Please enter %d numbers, seperated by either space, or new line)\n", AMOUNT);
    for (int i = 0; i < AMOUNT; i++)
        scanf("%d",&numbers[i]);

    // Calculate the min, max sum avg
    min = numbers[0];
    max = numbers[0];
    for (int i = 0; i < AMOUNT; i++) {
        sum += numbers[i];
        min = numbers[i] < min ? numbers[i] : min;
        max = numbers[i] > max ? numbers[i] : max;
    }
    avg = (float) sum / AMOUNT;

    // Print the stuff
    printf("Sum of numbers:\t\t%5d\n",sum);
    printf("Minimum of numbers:\t%5d\n",min);
    printf("Maximum of numbers:\t%5d\n",max);
    printf("Average of numbers:\t%5g\n",avg);

    return 0;
}

int solution_without_arrays() {

    // Declare the variables
    int sum=0, min, max, current;
    float avg;

    // Get the values from the user and immideatelly process it
    printf("Please enter %d numbers, seperated by either space, or new line)\n", AMOUNT);
    for (int i = 0; i < AMOUNT; i++) {
        scanf("%d",&current);
        sum += current;
        min = i==0 ? current : current < min ? current : min;
        max = i==0 ? current : current > max ? current : max;
    }

    // Calculate the average
    avg = (float) sum / AMOUNT;

    // Print the stuff
    printf("Sum of numbers:\t\t%5d\n",sum);
    printf("Minimum of numbers:\t%5d\n",min);
    printf("Maximum of numbers:\t%5d\n",max);
    printf("Average of numbers:\t%5g\n",avg);

    return 0;
}
