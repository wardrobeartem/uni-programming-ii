#include <stdlib.h>
#include <stdio.h>

typedef struct linkedNode {
    int value;
    struct linkedNode *next;
} LinkedNode;

void linkedInsertLast(int);
void printLinkedList();

LinkedNode *head;

int main() {
    linkedInsertLast(1);
    linkedInsertLast(5);
    linkedInsertLast(7);



    printLinkedList();


}

void linkedInsertLast(int a) {
    if(head == NULL) {
        head = (LinkedNode*)(malloc(sizeof(LinkedNode)));
        head->value = a;
    } else {
        LinkedNode *tmp = head;
        while(tmp->next != NULL)
            tmp = tmp->next;
        LinkedNode *new = (LinkedNode*)(malloc(sizeof(LinkedNode)));
        new->value = a;
        tmp->next = new;
    }
}

void printLinkedList() {
    LinkedNode *tmp = head;
    while(tmp != NULL){
        printf("%d\n",tmp->value);
        tmp = tmp->next;
    }
}
