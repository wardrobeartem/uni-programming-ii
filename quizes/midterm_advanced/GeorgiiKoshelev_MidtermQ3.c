#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Function Prototypes go here
void populate_array(int[], int, int, int);
int array_sum(int[], int);
float array_avg(int, int);
int array_min_entry(int[], int);
int array_max_entry(int[], int);

int main(){

    // define an array to hold 25 numbers
    int array[25];

    // call your function to generate 25 integers whose values are between 0 and 49
    populate_array(array, 25, 0, 49);

    int sum = array_sum(array,25);

    // compute the average of the numbers stored in the array_avg
    float avg = array_avg(sum, 25);

    // find the minimum of th enumbers stored in the array
    int min = array_min_entry(array, 25);

    // find the maximum of the numbers stored in an array
    int max = array_max_entry(array, 25);



    // print the results
    printf("Sum: %d, Average: %g, Min: %d, Max: %d\n",sum,avg,min,max);


    return 0; // successful ending of the program



}// end of main function

// Function Definitions go here

void populate_array(int array[], int length, int min, int max) {
    srand(time(NULL));
    for (int i = 0; i < length; i++) {
        array[i] = (rand() % (max + 1 - min) + min);
    }
}

float array_avg(int sum, int length) {
    return (float) sum / length;
}

int array_min_entry(int array[], int length) {
    int min = array[0];
    for (int i = 0; i < length; i++) {
        if(array[i] < min) {
            min = array[i];
        }
    }
    return min;
}

int array_max_entry(int array[], int length) {
    int max = array[0];
    for (int i = 0; i < length; i++) {
        if(array[i] > max) {
            max = array[i];
        }
    }
    return max;
}

int array_sum(int array[], int length) {
    int sum = 0;
    for (int i = 0; i < length; i++) {
        sum += array[i];
    }
    return sum;
}
