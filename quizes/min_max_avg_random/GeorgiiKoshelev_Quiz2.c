#include <stdio.h>                        // Used for printing the output
#include <stdlib.h>                       // Used for random number generation
#include <time.h>                         // Used for time calculation
#include "GeorgiiKoshelevLibrary4Quiz2.h" // Has all the functions there

// Declare constants
const int ARRAY_LENGTH = 1000;
const int RANDOM_MAX = 49;
const int RANDOM_MIN = 1;

int main(int argc, char *argv[]) {

    // define an array to hold 6 numbers
    int random_numbers[ARRAY_LENGTH];

    // call number generator to populate the array with 1000 unique numbers between 1 and 54
    populate_array(random_numbers, ARRAY_LENGTH, RANDOM_MIN, RANDOM_MAX);

    // find the min of the numbers
    int min = array_min_entry(random_numbers, ARRAY_LENGTH);

    // find the max of the numbers
    int max = array_max_entry(random_numbers, ARRAY_LENGTH);

    // start the clock to record the begining time
    clock_t avg_calc_start_time = clock();

    // compute the average of numbers
    float avg = array_avg(random_numbers, ARRAY_LENGTH);

    // stopt the clock to record the ending time
    clock_t avg_calc_stop_time = clock();

    // Compute the time elapsed between the two clock times
    double time_difference_us = ((double)(avg_calc_stop_time - avg_calc_start_time) / CLOCKS_PER_SEC ) * 1000000;

    //Display the results
    printf("minimum: \t%d\n",min);
    printf("maximum: \t%d\n",max);
    printf("average: \t%g\n",avg);
    printf("avg time: \t%.2fμs\n",time_difference_us);

    return 0;
}
