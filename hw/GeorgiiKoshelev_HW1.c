#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define FILE_NAME           "Students_Data.txt"
#define FIELD_SEPARATOR     ";"
#define ADDRESS_SEPARATOR   ","
#define EOL_SEPARATOR       "."
#define ARRAY_LENGTH        100
#define PRINT_FIELDS        0x1ff

// street, number, zip, city, country

typedef char long_string [100];
typedef char string [20];

typedef struct{
    string street;
    unsigned int number;
    string zip;             //its not int for this reason: 04322-1424 <- this starts with 0 and has - in the middle
    string city;
    string country;
} Address;

typedef struct {
    string name;
    string lastname;
    unsigned int age;
    char gender;
    Address address;
} Person;

//function prototypes
Person parse_person(long_string);
Address parse_address(long_string);
void print_person(Person);
void print_people(Person*,int);
bool filter_person(Person*);

int main() {
    FILE *f = fopen(FILE_NAME,"r");
    if(f == NULL) {
        printf("couldn't open the file :(\n");
        exit(-1);
    }
    Person students[ARRAY_LENGTH];
    int number_of_students;
    long_string file_line;

    for(int i = 0; fgets(file_line, 99, f) != NULL; i++) {
        if(i==0)
            continue;
        Person new_person = parse_person(file_line);
        students[i-1] = new_person;
        number_of_students++;
    }

    print_people(students,number_of_students);
    fclose(f);

    return 0;
}

Person parse_person(long_string line) {
    Person new_person;
    char* token;
    token = strtok(line,FIELD_SEPARATOR);
    strcpy(new_person.name,token);
    token = strtok(NULL,FIELD_SEPARATOR);
    strcpy(new_person.lastname,token);
    token = strtok(NULL,FIELD_SEPARATOR);
    new_person.age = strtol(token,NULL,10);
    token = strtok(NULL,FIELD_SEPARATOR);
    new_person.gender = token[0];
    token = strtok(NULL,EOL_SEPARATOR);
    Address address = parse_address(token);
    new_person.address = address;
    return new_person;
}

Address parse_address(long_string line) {
    Address new_address;
    char* token;
    token = strtok(line,ADDRESS_SEPARATOR);
    strcpy(new_address.street,token);
    token = strtok(NULL,ADDRESS_SEPARATOR);
    new_address.number = strtol(token,NULL,10);
    token = strtok(NULL,ADDRESS_SEPARATOR);
    strcpy(new_address.zip,token);
    token = strtok(NULL,ADDRESS_SEPARATOR);
    strcpy(new_address.city,token);
    token = strtok(NULL,ADDRESS_SEPARATOR);
    strcpy(new_address.country,token);
    return new_address;
}

void print_person(Person person) {
    if(PRINT_FIELDS & 0x100) printf("Name:\t\t%s\n",person.name);
    if(PRINT_FIELDS & 0x080) printf("Surname:\t%s\n",person.lastname);
    if(PRINT_FIELDS & 0x040) printf("Age\t\t%d\n",person.age);
    if(PRINT_FIELDS & 0x020) printf("Gender:\t\t%c\n",person.gender);
    if(PRINT_FIELDS & 0x010) printf("Street:\t\t%s\n",person.address.street);
    if(PRINT_FIELDS & 0x008) printf("Number:\t\t%d\n",person.address.number);
    if(PRINT_FIELDS & 0x004) printf("Zip:\t\t%s\n",person.address.zip);
    if(PRINT_FIELDS & 0x002) printf("City:\t\t%s\n",person.address.city);
    if(PRINT_FIELDS & 0x001) printf("Country:\t%s\n",person.address.country);
    printf("\n");
}

void print_people(Person* students,int length) {
    unsigned int number_people_printed = 0;
    for(unsigned int i = 0; i < length && number_people_printed <= 3; i++) {
        if(!filter_person(&students[i])) {
            print_person(students[i]);
            number_people_printed++;
        }
    }
    if(number_people_printed == 0)
        printf("There is noone matching the criteria :(\n");
}

// returns true if the person should be filtered out from the output
bool filter_person(Person* person) {
    /*
    if(!(person->gender == 'M'))  // only display males
        return true;
    if(!(person->gender == 'M' && person->age > 25 && person->age < 25))  // only display males between 25 and 50
        return true;
    if((strcmp(person->address.city,"Berlin"))  // only display dudes from berlin
        return true;
    if((strcmp(person->address.city,"Berlin") && !(person->age = 30))  // only display dudes from berlin who are 30
        return true;
    */
    return false;
}
