#include <stdio.h>
#include <ctype.h>

size_t countAlphaConvertLowercase(char *string) {
    size_t numberOfCharacters = 0;
    for(int i = 0; string[i] != '\0' ; ++i) {
        string[i] = tolower(string[i]);
        numberOfCharacters += isalpha(string[i]) ? 1 : 0;
    }
    return numberOfCharacters;
}

size_t countDigits(char *string) {
    size_t numberOfCharacters = 0;
    for(int i = 0; string[i] != '\0' ; ++i) {
        numberOfCharacters += isdigit(string[i]) ? 1 : 0;
    }
    return numberOfCharacters;
}

size_t countNonAlphaNumeric(char *string) {
    size_t numberOfCharacters = 0;
    for(int i = 0; string[i] != '\0' ; ++i) {
        numberOfCharacters += !isalnum(string[i]) ? 1 : 0;
    }
    return numberOfCharacters;
}

int main() {
    char string [100];

    fgets(string, 99, stdin);
    printf("There are %zu alpha characters in  '%s'\n", countAlphaConvertLowercase(string), string);
	printf("There are %zu digits characters in '%s'\n", countDigits(string), string);
	printf("There are %zu other characters in  '%s'\n", countNonAlphaNumeric(string), string);
}
