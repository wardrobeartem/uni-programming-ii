#include <math.h>

const float E = 2.7182818284;

double f(float x) {
    if(x>0) {
        return 1+2*pow(E,(2*x-4));
    } else if(x==0) {
        return sin(x);
    } else {
        return 1/cosh(x);
    }
}
