#include <time.h>                   // Used to set the seed for the RNG

// Change each cell of array of the given length to a random number
void populate_array(int array[], int length, int min, int max) {
    /* generate a deck of non-repeating numbers
     * to pull ints for the array from there
     */
    int deck_size = max-min+1;
    int deck[deck_size];
    for (int i = 0; i < deck_size; i++) {
        deck[i] = (i+min);
    }

    srand(time(NULL));
    for (int i = 0; i < length; i++) {
        int random_entry = rand() % deck_size;
        array[i] = (deck[random_entry]);        // place an int from deck in array
        deck[random_entry] = deck[deck_size-1]; // remove that int from deck
        deck_size--;                            // shorten the deck
    }
}

// Print array of a given length in a form [ 1 2 3 4 5 ]
void print_array(int array[], int length) {
    printf("[");
    for (int i = 0; i < length; i++) {
        printf(" %d",array[i]);
    }
    printf(" ]\n");
}

// Bubble sort i think?
void sort_array(int array[], int length) {
    int tmp;
    for (int i = 0; i < length; i++) {
        for (int j = i+1; j < length; j++) {
            if(array[i] > array[j]) {
                tmp = array[j];
                array[j] = array[i];
                array[i] = tmp;
            }
        }
    }

}
