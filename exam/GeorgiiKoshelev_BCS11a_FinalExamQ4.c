#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define MAX_ENTRIES     50
#define PRINT_ENTRIES   5
#define LINE_LENGTH     100
#define FILE_NAME       "CourseData.txt"
#define FIELD_DELIMITER ";"
#define EOL_DELIMITER   "."

#define LSTRIP(x) x += strspn(x, " ") // macro to strip leading spaces

typedef struct {
    char    courseCode      [10];
    char    courseName      [30];
    char    instructorName  [30];
    size_t  numStudents;
} Course;

Course Courses [MAX_ENTRIES];
int numberOfEntries = 0;

Course  parseLine(char*);
void    insertCourse(Course);
void    printCourse(Course);
void    printCourses();
bool    filterCourse(Course);


int main() {

    FILE *f = fopen(FILE_NAME, "r");
    if(f == NULL) {
        fprintf(stderr, "\033[31mCan't open '%s': %m\n", FILE_NAME); // \033[31m means make text RED
        exit(-1);
    }

    char line [LINE_LENGTH];

    fgets(line, LINE_LENGTH-1, f); // disregard the first line

    while(!feof(f)) {
        fgets(line, LINE_LENGTH-1, f);
        insertCourse(parseLine(line));
    }

    printCourses();

    fclose(f);
    return 0;
}

Course parseLine(char* a) {
    Course newCourse;
    char *token;

    token = strtok(a, FIELD_DELIMITER);
    LSTRIP(token);
    strcpy(newCourse.courseCode, token);

    token = strtok(NULL, FIELD_DELIMITER);
    LSTRIP(token);
    strcpy(newCourse.courseName, token);

    token = strtok(NULL, FIELD_DELIMITER);
    LSTRIP(token);
    strcpy(newCourse.instructorName, token);

    token = strtok(NULL, EOL_DELIMITER);
    newCourse.numStudents = strtol(token, NULL, 10);

    return newCourse;
}

void insertCourse(Course a) {
    Courses[numberOfEntries] = a;
    numberOfEntries++;
}

void printCourse(Course a) {
    printf("Course Code:\t\t%s\n",a.courseCode);
    printf("Course Name:\t\t%s\n",a.courseName);
    printf("Instructor:\t\t%s\n",a.instructorName);
    printf("Number of Students:\t%zu\n\n",a.numStudents);

}

void printCourses() {
    for(size_t i = 0, entriesPrinted = 0; i < numberOfEntries && entriesPrinted < PRINT_ENTRIES; i++)
        if(filterCourse(Courses[i])){
            printCourse(Courses[i]);
            entriesPrinted++;
        }
}

bool filterCourse(Course a) {

    return a.numStudents > 25;

}