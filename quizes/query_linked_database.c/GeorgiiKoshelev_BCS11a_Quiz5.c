//  ____
// < hi >
//  ----
//         \   ^__^
//          \  (oo)\_______
//             (__)\       )\/\
//                 ||----w |
//                 ||     ||
//
//
//
// created by Mark Zukenberg :)
// 01-01-1970


#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILE_NAME           "Students.txt"
#define PRINT_LIMIT         10
#define FIELD_SEPARATOR     ";"
#define EOL_SEPARATOR       "."

#define LSTRIP(x) x += strspn(x, " ") // macro to strip leading spaces

enum PRINT_FLAGS    {Name = 8, Lastname = 4, Age = 2, Gender = 1}; // sorry I just like doing this
const enum PRINT_FLAGS PRINT_FIELDS = Name | Lastname | Age | Gender; // Specifies which fields should be printed. Putting 0 prints nothing

typedef char string   [20];
typedef char sentence [100];

typedef struct {
    string  name;
    string  lastname;
    size_t  age;
    char    gender;
} Student;

typedef struct linkedNode {
    Student data;
    struct  linkedNode *next;
} LinkedNode;

// function prototypes
Student parseStudent(sentence);
void    insert(LinkedNode**, Student);
void    printStudent(Student);
void    printHeaders();
void    printList(LinkedNode*);
bool    checkStudent(Student);

int main() {

    FILE *f = fopen(FILE_NAME, "r");
    if(f == NULL) {
        fprintf(stderr, "Can't open '%s': %m\n", FILE_NAME);
        exit(-1);
    }

    LinkedNode *head = NULL;
    sentence line;

    fgets(line, 100, f); // disregard the first line

    while(!feof(f)) {
        fgets(line, 100, f);
        insert(&head, parseStudent(line));
    }

    printList(head);

    fclose(f);
    return 0;
}

Student parseStudent(sentence line) {
    Student newStudent;
    char *token;

    token = strtok(line, FIELD_SEPARATOR);
    LSTRIP(token);
    strcpy(newStudent.name, token);

    token = strtok(NULL, FIELD_SEPARATOR);
    LSTRIP(token);
    strcpy(newStudent.lastname, token);

    token = strtok(NULL, FIELD_SEPARATOR);
    newStudent.age = strtol(token, NULL, 10); // this does not need LSTRIP because strtol disregards all non-number symbols into NULL, in this case (the second argument specifies where all non-number characters should end up)

    token = strtok(NULL, EOL_SEPARATOR);
    LSTRIP(token);
    newStudent.gender = *token;

    return newStudent;
}

void insert(LinkedNode** head, Student freshman) {
    LinkedNode *prvNode = NULL;
    LinkedNode *curNode = *head;
    LinkedNode *newNode = (LinkedNode*) malloc(sizeof(LinkedNode));
    newNode->data = freshman;

    while(curNode != NULL && strcmp(curNode->data.lastname, freshman.lastname) < 0) { // sorting does here
        prvNode = curNode;
        curNode = curNode->next;
    }

    if(prvNode == NULL) { // if inserting to the beginning of the list
        newNode->next = curNode;
        *head = newNode;
    } else { // if inserting to any other position
        newNode->next = curNode;
        prvNode->next = newNode;
    }
}

void printStudent(Student greg) { // variable names are hard
    if(PRINT_FIELDS & 0x08) printf("%-20s\t", greg.name);
    if(PRINT_FIELDS & 0x04) printf("%-20s\t", greg.lastname);
    if(PRINT_FIELDS & 0x02) printf("%-3zu\t", greg.age);
    if(PRINT_FIELDS & 0x01) printf("%-c",     greg.gender);
    if(PRINT_FIELDS)        printf("\n");
}

void printHeaders() {
    if(PRINT_FIELDS & 0x08) printf("%-20s\t", "Name");
    if(PRINT_FIELDS & 0x04) printf("%-20s\t", "Lastname");
    if(PRINT_FIELDS & 0x02) printf("%-3s\t",  "Age");
    if(PRINT_FIELDS & 0x01) printf("%-s",     "Gender");
    if(PRINT_FIELDS)        printf("\n\n");
}

void printList(LinkedNode* head) {
    LinkedNode *curNode = head;

    size_t numberOfStudentsPrinted = 0;

    printHeaders();
    while(curNode != NULL && numberOfStudentsPrinted < 10) {
        if(checkStudent(curNode->data)) {
            printStudent(curNode->data);
            ++numberOfStudentsPrinted;
        }
        curNode = curNode->next;
    }

    if(numberOfStudentsPrinted == 0) {
        printf("There is nobody matching the criteria :(\n");
    }

    // printf("\nNumber of students printed: %zu\n", numberOfStudentsPrinted);
}

// returns true if the student should be printed
bool checkStudent(Student greg) {
    return greg.age <= 27 && greg.age >= 18 && greg.gender == 'F';
}
