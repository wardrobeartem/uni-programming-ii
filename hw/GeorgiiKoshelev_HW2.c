//
// created by Mario Ebersbacher (Georgii's Best friend)
// Mario has left this comment here in hopes that Georgii doesn't notice :)
// He asked me to do every one of his homeworks so I got angy now :(
//


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define FILE_NAME       "Students.txt"
#define FIELD_SEPARATOR " "
#define PRINT_ENTRIES   3

enum BOOLEAN        {FALSE, TRUE};
enum PRINT_FLAGS    {Name = 16, Lastname = 8, Gender = 4, Age = 2, Grade = 1};
const enum PRINT_FLAGS PRINT_FIELDS = Name | Lastname | Gender | Age | Grade; // Specifies which fields should be printed

typedef char longString [80];
typedef char string [40];

typedef struct {
    string Name;
    string Lastname;
    char Gender;
    unsigned int Age;
    float Grade;
} Student;

typedef struct linkedNode {
    Student value;
    struct linkedNode *next;
} LinkedNode;

// function prototypes
Student parseStudent(longString);
void    printStudent(Student);
void    printHeaders();
void    printList(LinkedNode*);
void    insertStudentLast(Student, LinkedNode**);
int     checkStudent(Student);

int main() {

    // open the file and check if it opened successfully
    FILE *f = fopen(FILE_NAME,"r");
    if(f == NULL) {
        fprintf(stderr, "Can't open '%s': %m\n", FILE_NAME);
        exit(-1);
    }

    // initialise the head pointer of the linked list
    LinkedNode *head;
    head = NULL;

    // scan the file line by line, parse and store the results inside of the linked list
    longString line;
    for(int i = 0; fgets(line, 80, f) != NULL; i++) {
        if(i==0)
            continue;
        Student newStudent = parseStudent(line);
        insertStudentLast(newStudent, &head);
    }

    // print what you got
    printList(head);

    return 0;
}

Student parseStudent(longString a) {
    Student newStudent;
    char* token;
    token = strtok(a,FIELD_SEPARATOR);
    strcpy(newStudent.Name,token);
    token = strtok(NULL,FIELD_SEPARATOR);
    strcpy(newStudent.Lastname,token);
    token = strtok(NULL,FIELD_SEPARATOR);
    newStudent.Gender = token[0];
    token = strtok(NULL,FIELD_SEPARATOR);
    newStudent.Age = strtol(token,NULL,10);
    token = strtok(NULL,FIELD_SEPARATOR);
    newStudent.Grade = strtold(token,NULL);
    return newStudent;
}

void printStudent(Student a) {
    if(PRINT_FIELDS & 0x10) printf("%-10s\t", a.Name);
    if(PRINT_FIELDS & 0x08) printf("%-10s\t", a.Lastname);
    if(PRINT_FIELDS & 0x04) printf("%-6c\t",  a.Gender);
    if(PRINT_FIELDS & 0x02) printf("%-5d\t",  a.Age);
    if(PRINT_FIELDS & 0x01) printf("%-5.1f\t",a.Grade);

    printf("\n");

    return;
}

void printHeaders() {
    if(PRINT_FIELDS & 0x10) printf("%-10s\t","Name");
    if(PRINT_FIELDS & 0x08) printf("%-10s\t","Lastname");
    if(PRINT_FIELDS & 0x04) printf("%-6s\t", "Gender");
    if(PRINT_FIELDS & 0x02) printf("%-5s\t", "Age");
    if(PRINT_FIELDS & 0x01) printf("%-5s\t", "Grade");

    printf("\n\n");

    return;

}

void printList(LinkedNode *head) {
    printHeaders();
    LinkedNode *tmp = head;
    int numberOfStudentsPrinted = 0;
    for(;tmp != NULL && numberOfStudentsPrinted < 3;tmp = tmp->next){
        if (!checkStudent(tmp->value))
            continue;
        printStudent(tmp->value);
        ++numberOfStudentsPrinted;
    }
    if(numberOfStudentsPrinted == 0)
        printf("There is no students fitting the criteria :(");

    return;
}

void insertStudentLast(Student a, LinkedNode **headPtr) {

    LinkedNode *new = (LinkedNode*) malloc(sizeof(LinkedNode));
    new->value = a;

    if(*headPtr == NULL) {
        *headPtr = new;
    } else {
        LinkedNode *tmp = *headPtr;
        for(;tmp->next != NULL;tmp = tmp->next);
        tmp->next = new;
    }

    return;
}

// returns true if the student should be printed
int checkStudent(Student a) {
    return TRUE;
}
