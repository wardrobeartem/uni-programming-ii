unsigned long long factorial_header(int a) {
    unsigned long long factorial = 1;
    if(a <= 1) {
        return 1;
    }
    for(int i = 1; i <= a; i++) {
        factorial = factorial * i;
    }
    return factorial;
}
