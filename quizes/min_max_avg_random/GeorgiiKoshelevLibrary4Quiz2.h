#include <time.h>                         // Used to set the seed for the RNG

// Change each cell of array of the given length to a random number
void populate_array(int array[], int length, int min, int max) {
    srand(time(NULL));
    for (int i = 0; i < length; i++) {
        array[i] = (rand() % (max + 1 - min) + min);
    }
}

// Return the minimum value of all entries of the array
int array_min_entry(int array[], int length) {
    int min = array[0];
    for (int i = 0; i < length; i++) {
        if(array[i] < min) {
            min = array[i];
        }
    }
    return min;
}

// Return the maximum value of all entries of the array
int array_max_entry(int array[], int length) {
    int max = array[0];
    for (int i = 0; i < length; i++) {
        if(array[i] > max) {
            max = array[i];
        }
    }
    return max;
}

// Return the average (arithmetic mean) of the values of all entries of the array
float array_avg(int array[], int length) {
    int sum = 0;
    for (int i = 0; i < length; i++) {
        sum += array[i];
    }
    return (float) sum / length;
}

